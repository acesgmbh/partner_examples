#!/bin/sh

NOW=$(date +"%T")
echo "Start aggregating server data at ${NOW}" >> /dev/stdout
cd /usr/src/app/ && npm run aggregate
