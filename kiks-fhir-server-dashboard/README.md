# AIQNET FHIR Server Dashboard
This application collects different kind of metadata from a FHIR server and displays it for the user. Data is aggregated at every full hour.
## Usage
1. Specify the target server in `server/config.js`: 
```js
SERVER: 'my-fhir-server-url.com',
```
2. Run the application with `docker-compose up -d`, stop it with `docker-compose down`

3. The dashboard is available via http://localhost:7777 and updated automatically.
