const AppDAO = require("./src/dao");
const KIKSSample = require("./src/kikssample");
const Repository = require("./src/repository");
const FHIR = require("./src/fhir")

const FHIR_URL = "http://localhost:8080/kiks-fhir-server/fhir/";

const fhir = new FHIR(FHIR_URL);
const db = new AppDAO();
const repository = new Repository(db, fhir);

const app = new KIKSSample(repository);

app.start();
