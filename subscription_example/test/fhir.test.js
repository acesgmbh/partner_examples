const chai  = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const fs = require('fs');
const path = require('path');

let FHIR = require("../src/fhir")
let fhirServer = new FHIR('test');

function readFixture(filename) {
    return JSON.parse(fs.readFileSync(path.normalize(`${__dirname}/fixtures/${filename}`, 'utf8')));
}

describe('FHIR testing', function() {
    afterEach(function () {
      sinon.restore();
    });

    it('Error in case resource is not found', async function () {
        sinon.stub(fhirServer, 'getEncounter').returns({});
        sinon.stub(fhirServer, 'getPatient').returns({});
        sinon.stub(fhirServer, 'getProcedure').returns({});
        sinon.stub(fhirServer, 'getCondition').returns({});
        sinon.stub(fhirServer, 'getReferrer').returns({});

        let result;
        try {
            result = await  fhirServer.buildNarrative('123');
        } catch( error ) {
            expect( error.message ).to.equal('Cannot read property \'reference\' of undefined');
        }
    });

    
    it('Success in case resources found', async function () {
        sinon.stub(fhirServer, 'getEncounter').returns(readFixture('encounter.json'));
        sinon.stub(fhirServer, 'getPatient').returns(readFixture('patient.json'));
        sinon.stub(fhirServer, 'getProcedure').returns(readFixture('procedure.json'));
        sinon.stub(fhirServer, 'getCondition').returns(readFixture('condition.json'));
        sinon.stub(fhirServer, 'getReferrer').returns(readFixture('practitioner.json'));

        let result = await fhirServer.buildNarrative('123');
        expect( result ).to.have.string('Patient Baum, male was admitted');
    });
});