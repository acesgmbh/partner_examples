const sqlite3 = require("sqlite3");
const Promise = require("bluebird");

class AppDAO {
  constructor() {
    this.db = new sqlite3.Database(":memory:", (err) => {
      if (err) {
        return console.error(err.message);
      }
    });

    this.db.run("CREATE TABLE pats(data text)");
  }

  saveNarrative(narrative) {
    this.db.run(`INSERT INTO pats(data) VALUES(?)`,[narrative]);
  }
}

module.exports = AppDAO;
